package com.ramalyusifov.walletapp.data.repositories

import com.ramalyusifov.walletapp.data.entities.Transaction
import com.ramalyusifov.walletapp.data.entities.TransactionType
import kotlinx.coroutines.flow.Flow

interface TransactionsRepository {

    suspend fun getTransactions(transactionType: TransactionType): Flow<List<Transaction>>

    suspend fun delete(transaction: Transaction)

    suspend fun getTotalAmount(): Flow<Double>
}