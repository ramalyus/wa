package com.ramalyusifov.walletapp.data.entities

import java.util.Date

data class Transaction(
    val id: Int,
    val type: TransactionType,
    val title: String,
    val description: String,
    val amount: Double,
    val date: Date
)