package com.ramalyusifov.walletapp.data.repositories.mock

import com.ramalyusifov.walletapp.data.repositories.AuthRepository

class MockAuthRepository : AuthRepository {

    override suspend fun isAuthorised() = true
}