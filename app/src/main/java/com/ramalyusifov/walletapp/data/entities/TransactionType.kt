package com.ramalyusifov.walletapp.data.entities

enum class TransactionType {
    INCOME,
    UNDEFINED,
    EXPENSES
}