package com.ramalyusifov.walletapp.data.repositories

interface AuthRepository {

    suspend fun isAuthorised(): Boolean
}