package com.ramalyusifov.walletapp.data.repositories.mock

import com.ramalyusifov.walletapp.data.entities.Transaction
import com.ramalyusifov.walletapp.data.entities.TransactionType
import com.ramalyusifov.walletapp.data.repositories.TransactionsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.Date

class MockTransactionsRepository : TransactionsRepository {

    private val transactionList = mutableListOf<Transaction>()

    init {
        populateTransactionList()
    }

    private val totalStateFlow = MutableStateFlow(calculateTotalAmount())

    private val incomesStateFlow = MutableStateFlow(filterTransactions(TransactionType.INCOME))

    private val allStateFlow = MutableStateFlow(filterTransactions(TransactionType.UNDEFINED))

    private val expensesStateFlow = MutableStateFlow(filterTransactions(TransactionType.EXPENSES))

    override suspend fun getTransactions(
        transactionType: TransactionType
    ): StateFlow<List<Transaction>> {

        return when (transactionType) {
            TransactionType.INCOME -> incomesStateFlow
            TransactionType.UNDEFINED -> allStateFlow
            TransactionType.EXPENSES -> expensesStateFlow
        }
    }

    override suspend fun delete(transaction: Transaction) {
        transactionList.remove(transaction)

        allStateFlow.value = filterTransactions(TransactionType.UNDEFINED)
        if (transaction.type == TransactionType.INCOME) {
            incomesStateFlow.value = filterTransactions(TransactionType.INCOME)
        } else {
            expensesStateFlow.value = filterTransactions(TransactionType.EXPENSES)
        }

        totalStateFlow.value = calculateTotalAmount()
    }

    override suspend fun getTotalAmount() = totalStateFlow

    private fun filterTransactions(transactionType: TransactionType): List<Transaction> {
        return if (transactionType == TransactionType.UNDEFINED) {
            transactionList.toMutableList()
        } else {
            transactionList.filter { it.type == transactionType }.toMutableList()
        }
    }

    private fun calculateTotalAmount(): Double {
        return transactionList.sumByDouble {
            if (it.type == TransactionType.INCOME) it.amount else -it.amount
        }
    }

    private fun populateTransactionList() {
        transactionList.add(
            Transaction(
                id = 1,
                type = TransactionType.EXPENSES,
                title = "New smartphone",
                description = "I want to buy a new smartphone cause mine is broken(",
                date = Date(),
                amount = 600.0
            )
        )

        transactionList.add(
            Transaction(
                id = 2,
                type = TransactionType.INCOME,
                title = "Salary",
                description = "Finally it's here",
                date = Date(),
                amount = 1000.0
            )
        )

        transactionList.add(
            Transaction(
                id = 3,
                type = TransactionType.EXPENSES,
                title = "Food",
                description = "I need to eat something you know",
                date = Date(),
                amount = 45.50
            )
        )
    }
}