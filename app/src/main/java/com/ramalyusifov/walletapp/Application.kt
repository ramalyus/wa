package com.ramalyusifov.walletapp

import android.app.Application
import com.ramalyusifov.walletapp.ui.base.ViewModelFactory

class Application : Application() {
    
    val viewModelFactory = ViewModelFactory()
}