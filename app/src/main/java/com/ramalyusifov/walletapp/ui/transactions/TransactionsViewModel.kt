package com.ramalyusifov.walletapp.ui.transactions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ramalyusifov.walletapp.data.entities.Transaction
import com.ramalyusifov.walletapp.data.entities.TransactionType
import com.ramalyusifov.walletapp.data.repositories.TransactionsRepository
import com.ramalyusifov.walletapp.ui.base.BaseViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TransactionsViewModel(
    private val repository: TransactionsRepository
) : BaseViewModel() {

    private val _transactionsLiveData = MutableLiveData<List<Transaction>>()
    val transactionsLiveData: LiveData<List<Transaction>> = _transactionsLiveData

    private val _totalLiveData = MutableLiveData<Double>()
    val totalLiveData: LiveData<Double> = _totalLiveData

    fun loadTransactions(transactionType: TransactionType) {
        scope.launch {
            startProgressAnimation()
            repository.getTransactions(transactionType).collect { value ->
                _transactionsLiveData.postValue(value)
                endProgressAnimation()
            }
        }
    }

    fun deleteTransaction(item: Transaction) {
        scope.launch {
            repository.delete(item)
        }
    }

    fun subscribeToTransactionTotal() {
        scope.launch {
            repository.getTotalAmount().collect { value ->
                _totalLiveData.postValue(value)
            }
        }
    }
}
