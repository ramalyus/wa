package com.ramalyusifov.walletapp.ui.splash

import androidx.lifecycle.LiveData
import com.ramalyusifov.walletapp.ui.base.BaseViewModel
import com.ramalyusifov.walletapp.data.repositories.AuthRepository
import com.ramalyusifov.walletapp.util.SingleLiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(private val authRepository: AuthRepository) : BaseViewModel() {

    private val toAuthScreenEvent = SingleLiveEvent<Unit>()
    val toAuthScreenLiveData: LiveData<Unit> = toAuthScreenEvent

    private val toTransactionsScreenEvent = SingleLiveEvent<Unit>()
    val toTransactionsScreenLiveData: LiveData<Unit> = toTransactionsScreenEvent

    init {
        isUserAuthorised()
    }

    private fun isUserAuthorised() {
        scope.launch {
            val isAuthorised = async {
                authRepository.isAuthorised()
            }
            val timeToWait = async {
                delay(SPLASH_DELAY)
            }
            timeToWait.await()

            if (isAuthorised.await()) {
                toTransactionsScreenEvent.postValue(Unit)
            } else {
                toAuthScreenEvent.postValue(Unit)
            }
        }
    }

    companion object {

        private const val SPLASH_DELAY = 2000L
    }
}