package com.ramalyusifov.walletapp.ui.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.plus

abstract class BaseViewModel : ViewModel() {

    protected val handler = CoroutineExceptionHandler { _, t ->
        Log.e(TAG, "Unhandled exception", t)
    }

    protected val scope = viewModelScope + Dispatchers.IO + handler

    private val _showProgressLiveData = MutableLiveData<Boolean>()
    val showProgressLiveData: LiveData<Boolean> = _showProgressLiveData

    protected fun startProgressAnimation() {
        _showProgressLiveData.postValue(true)
    }

    protected fun endProgressAnimation() {
        _showProgressLiveData.postValue(false)
    }

    companion object {
        private val TAG = BaseViewModel::class.java.canonicalName
    }
}