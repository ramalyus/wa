package com.ramalyusifov.walletapp.ui.transactions.list

import android.view.View
import androidx.core.content.ContextCompat
import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.base.BaseAdapter
import com.ramalyusifov.walletapp.data.entities.Transaction
import com.ramalyusifov.walletapp.data.entities.TransactionType
import com.ramalyusifov.walletapp.ui.base.BaseViewHolder
import com.ramalyusifov.walletapp.util.date.DateFormatter
import kotlinx.android.synthetic.main.item_transaction.transactionAmount
import kotlinx.android.synthetic.main.item_transaction.transactionDate
import kotlinx.android.synthetic.main.item_transaction.transactionTitle

class TransactionsAdapter(
    private val onTransactionClickListener: (Transaction) -> Unit
) : BaseAdapter<Transaction, TransactionsAdapter.TransactionViewHolder>() {

    private val dateFormatter = DateFormatter()

    override fun getViewHolder(view: View, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(view, onTransactionClickListener)
    }

    override fun getLayoutId(viewType: Int) = R.layout.item_transaction

    fun updateTransactions(transactionsList: List<Transaction>) {
        setItems(transactionsList)
    }

    inner class TransactionViewHolder(itemView: View) : BaseViewHolder<Transaction>(itemView) {

        private var transaction: Transaction? = null

        constructor(
            itemView: View,
            onItemClickListener: (Transaction) -> Unit
        ) : this(itemView) {

            containerView.setOnClickListener {
                if (transaction != null) onItemClickListener(transaction as Transaction)
            }
        }

        override fun bind(item: Transaction) {
            transaction = item
            transactionTitle.text = item.title
            transactionDate.text = dateFormatter.toReadable(item.date)
            setAmount(item)
        }

        private fun setAmount(item: Transaction) {
            val (formatString, colorRes) = if (item.type == TransactionType.INCOME) {
                Pair(R.string.income_format, R.color.colorIncome)
            } else {
                Pair(R.string.expenses_format, R.color.colorExpenses)
            }
            transactionAmount.text = itemView.context.getString(formatString, item.amount)
            transactionAmount.setTextColor(ContextCompat.getColor(itemView.context, colorRes))
        }
    }
}