package com.ramalyusifov.walletapp.ui.auth

import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.base.BaseActivity

class AuthActivity : BaseActivity<AuthViewModel>(R.layout.activity_auth)