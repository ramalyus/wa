package com.ramalyusifov.walletapp.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.ramalyusifov.walletapp.util.getViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : BaseViewModel>(
    @LayoutRes contentLayoutId: Int
) : AppCompatActivity(contentLayoutId) {

    protected val viewModel by lazy {
        val superclass = (javaClass.genericSuperclass as ParameterizedType)
        val viewModelClass = superclass.actualTypeArguments[0] as Class<VM>
        getViewModelProvider(this).get(viewModelClass)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeViewModel()
    }

    @CallSuper
    protected open fun subscribeViewModel() { }
}
