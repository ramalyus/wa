package com.ramalyusifov.walletapp.ui.transactions

import android.os.Bundle
import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.base.BaseActivity
import com.ramalyusifov.walletapp.ui.transactions.list.TransactionsFragment
import com.ramalyusifov.walletapp.util.observe
import kotlinx.android.synthetic.main.activity_transactions.*

class TransactionsActivity : BaseActivity<TransactionsViewModel>(R.layout.activity_transactions) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            loadTransactionListFragment()
        }
    }

    override fun subscribeViewModel() {
        super.subscribeViewModel()

        viewModel.totalLiveData.observe(this) {
            transactionTotal.text = getString(R.string.transactions_total, it)
        }
        viewModel.subscribeToTransactionTotal()
    }

    private fun loadTransactionListFragment() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.transactionsFragmentContainer, TransactionsFragment.newInstance())
        }.commit()
    }
}
