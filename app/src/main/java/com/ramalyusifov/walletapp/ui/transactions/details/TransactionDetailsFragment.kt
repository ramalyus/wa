package com.ramalyusifov.walletapp.ui.transactions.details

import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.data.entities.Transaction
import com.ramalyusifov.walletapp.ui.base.BaseFragment

class TransactionDetailsFragment :
    BaseFragment<DetailsViewModel>(R.layout.fragment_transaction_details) {

    companion object {

        fun newInstance(transaction: Transaction) = TransactionDetailsFragment()
    }
}