package com.ramalyusifov.walletapp.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.auth.AuthActivity
import com.ramalyusifov.walletapp.ui.transactions.TransactionsActivity
import com.ramalyusifov.walletapp.ui.base.BaseActivity
import com.ramalyusifov.walletapp.util.observe
import kotlinx.android.synthetic.main.activity_splash.logoImage

class SplashActivity : BaseActivity<SplashViewModel>(R.layout.activity_splash) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        animateLogo()
    }

    private fun animateLogo() {
        logoImage.startAnimation(AnimationUtils.loadAnimation(this, R.anim.logo_fade_in))
    }

    private fun launchActivity(activity: Class<*>) {
        val intent = Intent(this@SplashActivity, activity)
        startActivity(intent)
        finish()
    }

    override fun subscribeViewModel() {
        super.subscribeViewModel()

        viewModel.toAuthScreenLiveData.observe(this) {
            launchActivity(AuthActivity::class.java)
        }

        viewModel.toTransactionsScreenLiveData.observe(this) {
            launchActivity(TransactionsActivity::class.java)
        }
    }
}