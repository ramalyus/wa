package com.ramalyusifov.walletapp.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ramalyusifov.walletapp.data.repositories.mock.MockAuthRepository
import com.ramalyusifov.walletapp.data.repositories.AuthRepository
import com.ramalyusifov.walletapp.data.repositories.TransactionsRepository
import com.ramalyusifov.walletapp.data.repositories.mock.MockTransactionsRepository
import com.ramalyusifov.walletapp.ui.splash.SplashViewModel
import com.ramalyusifov.walletapp.ui.transactions.TransactionsViewModel
import com.ramalyusifov.walletapp.ui.transactions.add.AddTransactionViewModel
import com.ramalyusifov.walletapp.ui.transactions.details.DetailsViewModel
import java.lang.IllegalStateException

class ViewModelFactory : ViewModelProvider.Factory {

    private val authRepository: AuthRepository by lazy {
        MockAuthRepository()
    }

    private val transactionsRepository : TransactionsRepository by lazy {
        MockTransactionsRepository()
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when(modelClass) {
            SplashViewModel::class.java -> SplashViewModel(authRepository)
            TransactionsViewModel::class.java -> TransactionsViewModel(transactionsRepository)
            DetailsViewModel::class.java -> DetailsViewModel()
            AddTransactionViewModel::class.java -> AddTransactionViewModel()
            else -> throw IllegalStateException("Wrong ViewModel class: ${modelClass.name}")
        } as T
    }
}