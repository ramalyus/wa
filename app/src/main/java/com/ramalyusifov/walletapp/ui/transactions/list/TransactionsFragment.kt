package com.ramalyusifov.walletapp.ui.transactions.list

import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayoutMediator
import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.base.BaseFragment
import com.ramalyusifov.walletapp.ui.transactions.TransactionsViewModel
import com.ramalyusifov.walletapp.ui.transactions.add.AddTransactionFragment
import kotlinx.android.synthetic.main.fragment_transactions.fab
import kotlinx.android.synthetic.main.fragment_transactions.tabLayout
import kotlinx.android.synthetic.main.fragment_transactions.transactionPager

class TransactionsFragment : BaseFragment<TransactionsViewModel>(R.layout.fragment_transactions) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            setUpViewPager(DEFAULT_TAB)
        } else {
            val selectedTab = savedInstanceState.getInt(KEY_TAB_INDEX, DEFAULT_TAB)
            setUpViewPager(selectedTab)
        }

        fab.setOnClickListener {
            loadFragment(R.id.transactionsFragmentContainer, AddTransactionFragment.newInstance())
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_TAB_INDEX, tabLayout.selectedTabPosition)
        super.onSaveInstanceState(outState)
    }

    private fun setUpViewPager(selectedTab: Int) {
        val pagerAdapter = ViewPagerAdapter(this)
        transactionPager.adapter = pagerAdapter

        TabLayoutMediator(tabLayout, transactionPager) { tab, position ->
            tab.text = getString(pagerAdapter.getTabTitle(position))
        }.attach()

        transactionPager.setCurrentItem(selectedTab, false)
    }

    companion object {

        private const val DEFAULT_TAB = 1

        private val KEY_TAB_INDEX = ViewPagerAdapter::class.java.canonicalName

        fun newInstance() = TransactionsFragment()
    }
}