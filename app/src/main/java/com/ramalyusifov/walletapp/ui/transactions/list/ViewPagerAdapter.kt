package com.ramalyusifov.walletapp.ui.transactions.list

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ramalyusifov.walletapp.data.entities.TransactionType

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val transactionFragments = mutableListOf<TransactionsListFragment>().apply {
        add(TransactionsListFragment.newInstance(TransactionType.INCOME))
        add(TransactionsListFragment.newInstance(TransactionType.UNDEFINED))
        add(TransactionsListFragment.newInstance(TransactionType.EXPENSES))
    }

    override fun getItemCount() = transactionFragments.size

    override fun createFragment(position: Int): Fragment {
        return transactionFragments[position]
    }

    fun getTabTitle(position: Int): Int = transactionFragments[position].getTitleString()
}