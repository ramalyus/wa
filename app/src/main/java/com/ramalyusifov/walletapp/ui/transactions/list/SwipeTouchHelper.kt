package com.ramalyusifov.walletapp.ui.transactions.list

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class SwipeTouchHelper(
    callback: (ViewHolder, Int) -> Unit
) : ItemTouchHelper(object : SimpleCallback(0, LEFT) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        target: ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        callback(viewHolder, direction)
    }
})