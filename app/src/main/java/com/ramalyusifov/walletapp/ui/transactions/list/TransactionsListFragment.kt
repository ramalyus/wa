package com.ramalyusifov.walletapp.ui.transactions.list

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.data.entities.TransactionType
import com.ramalyusifov.walletapp.ui.base.BaseFragment
import com.ramalyusifov.walletapp.ui.transactions.TransactionsViewModel
import com.ramalyusifov.walletapp.ui.transactions.details.TransactionDetailsFragment
import com.ramalyusifov.walletapp.util.observe
import kotlinx.android.synthetic.main.fragment_transactions_list.transactionsRecyclerView

class TransactionsListFragment :
    BaseFragment<TransactionsViewModel>(R.layout.fragment_transactions_list) {

    private val adapter = TransactionsAdapter {
        loadFragment(R.id.transactionsFragmentContainer, TransactionDetailsFragment.newInstance(it))
    }

    override fun subscribeViewModel() {
        super.subscribeViewModel()

        viewModel.transactionsLiveData.observe(viewLifecycleOwner) {
            adapter.updateTransactions(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerView()

        viewModel.loadTransactions(requireFilter())
    }

    fun getTitleString(): Int {
        return when(requireFilter()) {
            TransactionType.INCOME -> R.string.transactions_income
            TransactionType.UNDEFINED -> R.string.transactions_all
            TransactionType.EXPENSES -> R.string.transactions_expenses
        }
    }

    private fun setUpRecyclerView() {
        transactionsRecyclerView.adapter = adapter
        val divider = DividerItemDecoration(context, RecyclerView.VERTICAL)
        transactionsRecyclerView.addItemDecoration(divider)
        SwipeTouchHelper(::onSwipe).attachToRecyclerView(transactionsRecyclerView)
    }

    private fun requireFilter() = requireArguments().get(KEY_FILTER) as TransactionType

    private fun onSwipe(viewHolder: RecyclerView.ViewHolder, moveDirection: Int) {
        val item = adapter.getItem(viewHolder.adapterPosition)
        adapter.removeItem(viewHolder.adapterPosition)
        viewModel.deleteTransaction(item)
    }

    companion object {

        private val KEY_FILTER = TransactionType::class.java.canonicalName.toString()

        fun newInstance(filter: TransactionType): TransactionsListFragment {
            return TransactionsListFragment().apply {
                arguments = bundleOf(KEY_FILTER to filter)
            }
        }
    }
}