package com.ramalyusifov.walletapp.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private val items = mutableListOf<T>()

    abstract fun getViewHolder(view: View, viewType: Int): VH

    @LayoutRes abstract fun getLayoutId(viewType: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val layoutId = getLayoutId(viewType)
        val view = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return getViewHolder(view, viewType)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
    }

    fun setItems(items: Collection<T>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setItem(item: T, position: Int) {
        if (position in 0 until items.size) {
            items[position] = item
            notifyItemChanged(position)
        }
    }

    fun getItem(position: Int): T = items[position]

    fun addItem(position: Int, item: T) {
        if (position in 0..items.size) {
            items.add(position, item)
            notifyItemInserted(position)
        }
    }

    fun addItem(item: T) {
        addItem(items.size, item)
    }

    fun removeItem(position: Int) {
        if (position in 0 until items.size && items.isNotEmpty()) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun addItems(position: Int, items: Collection<T>) {
        if (position in 0..items.size) {
            this.items.addAll(position, items)
            notifyItemRangeInserted(position, this.items.size)
        }
    }

    fun addItems(items: Collection<T>) {
        addItems(this.items.size, items)
    }
}