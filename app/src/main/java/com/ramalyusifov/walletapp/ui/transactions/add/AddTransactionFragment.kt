package com.ramalyusifov.walletapp.ui.transactions.add

import com.ramalyusifov.walletapp.R
import com.ramalyusifov.walletapp.ui.base.BaseFragment

class AddTransactionFragment :
    BaseFragment<AddTransactionViewModel>(R.layout.fragment_add_transaction) {

    companion object {

        fun newInstance() = AddTransactionFragment()
    }
}