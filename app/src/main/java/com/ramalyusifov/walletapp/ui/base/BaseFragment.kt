package com.ramalyusifov.walletapp.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.ramalyusifov.walletapp.ui.common.ProgressFragment
import com.ramalyusifov.walletapp.util.getViewModelProvider
import com.ramalyusifov.walletapp.util.observe
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<VM: BaseViewModel>(
    @LayoutRes contentLayoutId: Int
): Fragment(contentLayoutId) {

    protected val viewModel by lazy {
        val superclass = (javaClass.genericSuperclass as ParameterizedType)
        val viewModelClass = superclass.actualTypeArguments[0] as Class<VM>
        requireContext().getViewModelProvider(this).get(viewModelClass)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeViewModel()
    }

    @CallSuper
    protected open fun subscribeViewModel() {
        viewModel.showProgressLiveData.observe(this) {
            if (it) showProgress() else hideProgress()
        }
    }

    fun loadFragment(@IdRes fragmentId: Int, fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(fragmentId, fragment)
            addToBackStack(null)
        }.commit()
    }

    private fun showProgress() {
        if (childFragmentManager.findFragmentByTag(ProgressFragment.TAG) == null) {
            ProgressFragment.newInstance().show(requireFragmentManager(), ProgressFragment.TAG)
        }
    }

    private fun hideProgress() {
        val progressDialog = requireFragmentManager()
            .findFragmentByTag(ProgressFragment.TAG) as? ProgressFragment
        progressDialog?.dismissAllowingStateLoss()
    }
}