package com.ramalyusifov.walletapp.util.date

import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Date

class DateFormatter : DateUtil {

    private val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

    override fun toReadable(date: Date): String = formatter.format(date)
}