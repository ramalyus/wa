package com.ramalyusifov.walletapp.util.date

import java.util.Date

interface DateUtil {

    fun toReadable(date: Date): String
}