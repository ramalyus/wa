package com.ramalyusifov.walletapp.util

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.ramalyusifov.walletapp.Application

fun Context.getViewModelProvider(owner: ViewModelStoreOwner): ViewModelProvider {
    val factory = (applicationContext as Application).viewModelFactory
    return ViewModelProvider(owner, factory)
}
